#!/bin/sh

tmux new -d -s GOBALU -n frontend
tmux new-pane -t GOBALU:1 -n

tmux split-window -h -t GOBALU:1
tmux send-keys -t GOBALU:1.1 "cd /home/agusdmb/gobalu/frontend/" C-m
tmux send-keys -t GOBALU:1.1 "ng serve" C-m

tmux send-keys -t GOBALU:1.2  "cd /home/agusdmb/gobalu/backend/app/" C-m
tmux send-keys -t GOBALU:1.2  "workon gobalu" C-m
tmux send-keys -t GOBALU:1.2  "./manage.py runserver" C-m

tmux attach-session -t GOBALU
