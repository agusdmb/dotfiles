#!/bin/sh

tmux new -d -s BACKEND -n backend

tmux send-keys -t BACKEND:1 "cd backend/" C-m
tmux send-keys -t BACKEND:1 "cd app/" C-m
tmux send-keys -t BACKEND:1 "vim" C-m
tmux split-window -v -t BACKEND:1
tmux resize-pane -t BACKEND:1.2 -y 10
tmux send-keys -t BACKEND:1.2 "cd backend/" C-m
tmux send-keys -t BACKEND:1.2 "workon mj" C-m
tmux send-keys -t BACKEND:1.2 "cd app/" C-m
tmux send-keys -t BACKEND:1.2 "python manage.py runserver 0.0.0.0:8000" C-m

tmux select-pane -t BACKEND:1.1
tmux select-pane -t BACKEND:2.1
tmux select-window -t BACKEND:2.1
tmux attach-session -t BACKEND
