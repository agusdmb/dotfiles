let mapleader=','

inoremap kj <Esc>
vnoremap kj <Esc>
cnoremap kj <Esc>

nnoremap <Leader>x :exit<CR>
nnoremap <Leader>w :update<CR>
nnoremap <Leader>q :quit<CR>
nnoremap <Leader>c :bdelete<CR>

nnoremap <F5> :source %<CR>

nnoremap <CR> o<Esc>

nnoremap <Leader>p "+p
nnoremap <Leader>y "+y

nnoremap ; :

nnoremap <S-q> <Nop>

" Navigation
nnoremap <C-h> <C-w><C-h>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>
nnoremap <Leader>v :vertical split<CR>
nnoremap <Leader>h :split<CR>

set number
set cursorline
set colorcolumn=80
