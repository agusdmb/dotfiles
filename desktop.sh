#!/bin/sh

tmux new -d -s DESKTOP -n Ranger
tmux new-window -t DESKTOP:2 -n Neovim

tmux send-keys -t DESKTOP:1 "ranger" C-m
tmux send-keys -t DESKTOP:2 "nvim" C-m

tmux attach-session -t DESKTOP
