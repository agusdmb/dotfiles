#!/bin/bash

###########
# BACKEND #
###########

tmux new -d -s backend -c /home/agusdmb/dev/DataScience -n 'neovim'

# tmux split-window -h -t backend:1 -c /home/agusdmb/dev/DataScience
tmux new-window -t backend:2 -c /home/agusdmb/dev/DataScience -n 'shell'
tmux new-window -t backend:9 -c /home/agusdmb/dev/DataScience -n 'flask'
tmux send-keys -t backend:1 "bass source setenv.sh" C-m
# tmux send-keys -t backend:1.1 "bass source setenv.sh" C-m
# tmux send-keys -t backend:1.2 "bass source setenv.sh" C-m
tmux send-keys -t backend:2 "bass source setenv.sh" C-m
tmux send-keys -t backend:9 "bass source setenv.sh" C-m

if [[ $1 == full ]]
then
    tmux send-keys -t backend:1 "vim" C-m
    # tmux send-keys -t backend:1.1 "vim" C-m
    tmux send-keys -t backend:2 "FLASK_DEBUG=1 FLASK_APP=src/vocalo/api/main_app_wsgi.py flask shell -i notebook.py" C-m
    tmux send-keys -t backend:9 "FLASK_DEBUG=1 python src/vocalo/api/main_app.py" C-m
fi

############
# FRONTEND #
############

tmux new -d -s frontend -c /home/agusdmb/dev/Frontend -n 'app'
# tmux new-window -t frontend:2 -c /home/agusdmb/dev/Frontend
tmux select-pane -t frontend:1

if [[ $1 == full ]]
then
    tmux send-keys -t frontend:1 "npm start" C-m
    # tmux select-pane -t frontend:2
fi

##########
# SERVER #
##########

tmux new -d -s server -c /home/agusdmb/dev/DataScience -n 'bastion'

if [[ $1 == full ]]
then
    tmux send-keys -t server:1 "ssh vpn-boostup-bastion" C-m
fi

###########
# POSTMAN #
###########

tmux new -d -s postman -c /home/agusdmb/dev/postman -n 'postman'
tmux send-keys -t postman:1 "vim" C-m

###########
# NOTES #
###########

tmux new -d -s notes -c /home/agusdmb/dev/notes -n 'notes'
tmux send-keys -t notes:1 "vim" C-m

tmux select-window -t backend:1
# tmux select-pane -t backend:1.1
# tmux resize-pane -Z -t backend:1.1
tmux attach-session -t backend
