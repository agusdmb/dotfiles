#!/bin/sh

tmux new -d -s IDE -n backend
tmux new-window -t IDE:2 -n frontend

tmux send-keys -t IDE:1 "cd backend/" C-m
tmux send-keys -t IDE:1 "cd app/" C-m
tmux send-keys -t IDE:1 "vim" C-m
tmux split-window -v -t IDE:1
tmux resize-pane -t IDE:1.2 -y 10
tmux send-keys -t IDE:1.2 "cd backend/" C-m
tmux send-keys -t IDE:1.2 "workon mj" C-m
tmux send-keys -t IDE:1.2 "cd app/" C-m
tmux send-keys -t IDE:1.2 "python manage.py runserver 0.0.0.0:8000" C-m

tmux send-keys -t IDE:2 "cd frontend/src/app/" C-m
tmux send-keys -t IDE:2 "vim" C-m
tmux split-window -v -t IDE:2
tmux resize-pane -t IDE:2.2 -y 10
tmux send-keys -t IDE:2.2 "cd frontend/src/app/" C-m
tmux send-keys -t IDE:2.2 "ng serve --host 0.0.0.0 --port 4200" C-m

tmux select-pane -t IDE:1.1
tmux select-pane -t IDE:2.1
tmux select-window -t IDE:2.1
tmux attach-session -t IDE
