set fish_greeting
set -Ux EDITOR "nvim"

alias bat="batcat"
alias vim="nvim"
alias vimd='vim (git diff --name-only develop)'

alias ev="vim ~/.config/nvim/init.lua"
alias et="vim ~/dotfiles/tmux.conf"
alias ez="vim ~/dotfiles/zshrc"
alias ei="vim ~/dotfiles/vocalo.sh"
alias ef="vim ~/dotfiles/config.fish"
alias ea="vim ~/dotfiles/alacritty.yml"

alias lh="ls -lh"

alias copy="xclip -selection clipboard"

alias tiga="tig --all"

alias gl="git pull"
alias gp="git push"
alias gpf="git push --force-with-lease"
alias gd="git diff"
alias gst="git status"
alias gcam="git commit -am"
alias gaa="git add --all"
alias gco="git checkout"
alias gcb="git checkout -b"
alias gcd="git checkout develop"
alias gcm="git checkout master"
alias grbd="git rebase develop"
alias grbc="git rebase --continue"
alias gstp="git stash pop"
alias gpsup="git push --set-upstream origin (git symbolic-ref --quiet --short HEAD)"

alias tksv="tmux kill-server"

alias note="nvim -c 'Neorg index'"

set FZF_DEFAULT_COMMAND "fdfind --type f"
set FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"
set FZF_ALT_T_COMMAND "$FZF_DEFAULT_COMMAND"
set FZF_ALT_C_COMMAND "fdfind --type d"

# function gh
#     xdg-open (git config remote.origin.url | sed "s/git@\(.*\):\(.*\).git/https:\/\/\1\/\2/")/tree/(git symbolic-ref --quiet --short HEAD)
# end

starship init fish | source
