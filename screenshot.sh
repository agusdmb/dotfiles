#!/bin/bash
timestamp=$(date '+%Y-%m-%d %H-%M-%S')
gnome-screenshot -a -f "/home/agusdmb/Pictures/Screenshots/Screenshot from $timestamp.png" && \
cat "/home/agusdmb/Pictures/Screenshots/Screenshot from $timestamp.png" | xclip -selection clipboard -t image/png
