# zmodload zsh/zprof

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

ZSH_THEME="gruvbox"
# SOLARIZED_THEME="dark"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# bindkey -v
set -o vi

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

source $ZSH/custom/plugins/alias-tips/alias-tips.plugin.zsh
source ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
export NVM_LAZY_LOAD=true

plugins=(
    alias-tips
    docker
    docker-compose
    git
    git-auto-fetch
    zsh-nvm
    pip
    tmux
    zsh-autosuggestions
    zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

# to get to work 'cd ../'
zstyle ':completion:*' special-dirs true

alias rm='gio trash'

# convenience aliases for editing configs
alias vim='nvim'
alias ev='vim ~/vimrc/init.vim'
alias et='vim ~/dotfiles/tmux.conf'
alias ez='vim ~/dotfiles/zshrc'
alias ei='vim ~/dotfiles/vocalo.sh'
# alias vocalo='./vocalo.sh'

alias lh='ls -lh'

alias copy='xclip -selection clipboard'

alias tiga='tig --all'

# FZF configs

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export FZF_DEFAULT_COMMAND="fdfind --type f"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_T_COMMAND="$FZF_DEFAULT_COMMAND"

export FZF_ALT_C_COMMAND="fdfind --type d"

export EDITOR=nvim

export PATH=$PATH:~/.local/bin

gh(){
  xdg-open $(git config remote.origin.url | sed "s/git@\(.*\):\(.*\).git/https:\/\/\1\/\2/")/$1$2
}

# Open current branch
alias gh='gh tree/$(git symbolic-ref --quiet --short HEAD )'

alias vimd='vim $(git diff --name-only develop)'

export LC_ALL=en_US.UTF-8

export PATH="/home/agusdmb/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

export MANPAGER="nvim +Man!"
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"

alias bot=/home/agusdmb/./bot.sh
alias hbot=/home/agusdmb/./hbot.sh

source ~/.config/nvim/plugged/gruvbox/gruvbox_256palette.sh

eval "$(starship init zsh)"

# zprof
